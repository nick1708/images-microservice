const express = require("express");
const bodyParse = require("body-parser");
const dotEnv = require("dotenv");
const morgan = require("morgan");

dotEnv.config({path: "./config.env"});

const app = express();

app.use(morgan('dev'));
app.use(bodyParse.json());
app.use(bodyParse.urlencoded({extend:false}));

app.listen(process.env.PORT || 3000, _ => {
    console.clear();
    console.log(`Server is running on port ${process.env.PORT}`);
});